﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;


namespace myContactManager.Controllers
{
    public class DataController : Controller
    {
        // GET: Data
        public ActionResult Index()
        {
            return View();
        }


        // GET: Data/GetAllContacts
        public JsonResult GetAllContacts()
        {
            var contactList = new List<Contact>();

            ContactManagerDBEntities dc = new ContactManagerDBEntities();

            // returns entire list of subjects
            try
            {
                contactList = dc.Contacts.ToList();
            }

            catch(Exception e)
            {
                return new JsonResult { Data = e.ToString(), JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
            
            return new JsonResult { Data = contactList, JsonRequestBehavior = JsonRequestBehavior.AllowGet };

        }


        // GET: Data/FindContact?value=
        public JsonResult FindContact(string value)
        {
            var contactList = new List<Contact>();

            ContactManagerDBEntities dc = new ContactManagerDBEntities();

            // returns contacts with either (FirstName or LastName or Tags) = value  
            try
            {
                contactList = dc.Contacts.Where(a => a.FirstName.Contains(value) || a.LastName.Contains(value) || a.Tags.Contains(value)).ToList();   
            }

            catch (Exception e)
            {
                return new JsonResult { Data = e.ToString(), JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }

            return new JsonResult { Data = contactList, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        // GET: Data/FindContactbyID?value=
        public JsonResult FindContactbyID(int value)
        {
            var contactList = new List<Contact>();

            ContactManagerDBEntities dc = new ContactManagerDBEntities();

            // returns contacts with either (FirstName or LastName or Tags) = value  
            try
            {
                contactList = dc.Contacts.Where(a => a.ContactID.Equals(value)).ToList();
            }

            catch (Exception e)
            {
                return new JsonResult { Data = e.ToString(), JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }

            return new JsonResult { Data = contactList, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }
        // POST: Data/NewContact?value=
        public JsonResult NewContact(string value)

        {
            Json.Content(value);
            JsonConvert
            String.Format()
            JsonResult param = new JsonResult();
            Json.Content
            string parsedValue;
            parsedValue = value;
            parsedValue = parsedValue.Replace(@"\", "");
            JavaScriptSerializer JSS = new JavaScriptSerializer();

            Contact c = JSS.Deserialize<Contact>(parsedValue);
            Contact test = new Contact();
            try
            {
                using (ContactManagerDBEntities dc = new ContactManagerDBEntities())
                {
                   
                    dc.Contacts.Add(c);
                    dc.SaveChanges();

                    test = dc.Contacts.OrderByDescending(a => a.ContactID).Take(1).FirstOrDefault();
                }
            }
            catch (Exception e)
            {
                return new JsonResult { Data = e.ToString(), JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
                      
            return new JsonResult { Data = test, JsonRequestBehavior = JsonRequestBehavior.AllowGet };

        }

        // POST: Data/DeleteContact?value=
        public JsonResult DeleteContact(int value)
        {
           

        
            Contact oldContactValues = new Contact();
            
            try
            {
                using (ContactManagerDBEntities dc = new ContactManagerDBEntities())
                {
                    oldContactValues = dc.Contacts.Find(value);
                    dc.Contacts.Remove(oldContactValues);
                    dc.SaveChanges();

                    
                }
            }
            catch (Exception e)
            {
                return new JsonResult { Data = e.ToString(), JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }

            return new JsonResult { Data = "Contact Removed", JsonRequestBehavior = JsonRequestBehavior.AllowGet };

        }

        // POST: Data/EditContact?value=
        public JsonResult EditContact(string value)
        {

            JavaScriptSerializer JSS = new JavaScriptSerializer();

            Contact newContactValues = JSS.Deserialize<Contact>(value);
            Contact oldContactValues = new Contact();



            try
            {
                using (ContactManagerDBEntities dc = new ContactManagerDBEntities())
                {
                    
                    oldContactValues = dc.Contacts.Find(newContactValues.ContactID);

                    if (oldContactValues !=null)
                    {
                        oldContactValues.ContactID = newContactValues.ContactID;
                        oldContactValues.FirstName = newContactValues.FirstName;
                        oldContactValues.LastName = newContactValues.LastName;
                        oldContactValues.Address = newContactValues.Address;
                        oldContactValues.Email = newContactValues.Email;
                        oldContactValues.Phone = newContactValues.Phone;
                        oldContactValues.Tags = newContactValues.Tags;
                        dc.SaveChanges();
                    }

                  
                }
            }
            catch (Exception e)
            {
                return new JsonResult { Data = e.ToString(), JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }

            return new JsonResult { Data = oldContactValues, JsonRequestBehavior = JsonRequestBehavior.AllowGet };

        }
    }
}